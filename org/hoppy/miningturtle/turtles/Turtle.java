package org.hoppy.miningturtle.turtles;

import org.hoppy.miningturtle.wrappers.Block;
import org.hoppy.miningturtle.wrappers.Rotation;

public class Turtle extends Block {

	private String name;
	private Rotation rotation;
	private boolean isRunning = false;
	private int fuelLevel = 0;

	public Turtle(String name, int x, int y, int z, Rotation rotation) {
		super(name, x, y, z);
		this.name = name;
		this.rotation = rotation;
	}

	public String getname() {
		return name;
	}

	public Rotation getRotation() {
		return rotation;
	}

	public int getFuelLevel() {
		return fuelLevel;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void boot() {
		if (fuelLevel > 0) {
			isRunning = true;
			System.out.println(name + " is running now.");
		} else
			System.out.println("Failed to boot " + name + " , No fuel left.");
	}

	public void shutdown() {
		isRunning = false;
		System.out.println(name + " has been shutdown.");
	}

	public void addFuel(int fuelAmount) {
		fuelLevel += fuelAmount;
	}

	public void moveForward() {
		
		switch (rotation) {
		case SOUTH:
			setY(getY() - 1);
			break;
		case WEST:
			setX(getX() - 1);
			break;
		case NORTH:
			setY(getY() + 1);
			break;
		case EAST:
			setX(getX() + 1);
			break;
		}
		fuelLevel--;
	}

	public void moveBackwards() {
		switch (rotation) {
		case SOUTH:
			setY(getY() + 1);
			break;
		case WEST:
			setX(this.getX() + 1);
			break;
		case NORTH:
			setY(getY() - 1);
			break;
		case EAST:
			setX(getX() - 1);
			break;
		}
		fuelLevel--;
	}

	public void moveUp() {
		setZ(getZ() + 1);
		fuelLevel--;
	}

	public void moveDown() {
		setZ(getZ() - 1);
		fuelLevel--;
	}

	public void turnRight() {
		switch (rotation) {
		case SOUTH:
			rotation = Rotation.WEST;
			break;
		case WEST:
			rotation = Rotation.NORTH;
			break;
		case NORTH:
			rotation = Rotation.EAST;
			break;
		case EAST:
			rotation = Rotation.SOUTH;
			break;
		}
	}

	public void turnLeft() {
		switch (rotation) {
		case SOUTH:
			rotation = Rotation.EAST;
			break;
		case WEST:
			rotation = Rotation.SOUTH;
			break;
		case NORTH:
			rotation = Rotation.WEST;
			break;
		case EAST:
			rotation = Rotation.NORTH;
			break;
		}
	}

	public boolean detectFront() {
		// detects in block in front
		return false;
	}

	public boolean detectUp() {
		// detects in block above
		return false;
	}

	public boolean detectDown() {
		// detects in block underneath
		return false;
	}

	public void digFront() {
		// Digs block in front
	}

	public void digUp() {
		// Digs block above
	}

	public void digDown() {
		// Digs block underneath
	}
}
