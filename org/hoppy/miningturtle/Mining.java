package org.hoppy.miningturtle;

import org.hoppy.miningturtle.turtles.Turtle;
import org.hoppy.miningturtle.wrappers.Rotation;

/**
 * This turtle API is just a small program for Muggy21 at CarbonNetworks
 *
 * Info: - This is just some example code, this will not run in the game. - I
 * didn't use minecraft coordinate system, switched Y and Z.
 *
 * @author H0ppy
 * @version 1.0
 * @since 2015-12-18
 */

public class Mining {

	public static void main(String[] args) {

		// make a new turtle (same as placing the block in the game)
		Turtle miningTurtle = new Turtle("Squirtle", 50, 50, 64, Rotation.NORTH);

		// Add some fuel to the turtle
		miningTurtle.addFuel(325);

		// let's start the turtle
		miningTurtle.boot();

		// let's mine out a 2x2 area to bedrock

		// I'm pretty sure there are better algoritms to solve this, this is
		// just an exmaple on how to use.
		while (miningTurtle.getZ() > 4) {
			for (int j = 0; j < 4; j++) {
				if (miningTurtle.detectFront()) {
					miningTurtle.digFront();
					miningTurtle.moveForward();
					miningTurtle.turnLeft();
				} else {
					miningTurtle.moveForward();
					miningTurtle.turnLeft();
				}
			}

			// Lowers 1 block when mined the 2x2 area.
			if (miningTurtle.detectDown()) {
				miningTurtle.digDown();
				miningTurtle.moveDown();
			} else
				miningTurtle.moveDown();
		}

		// Shutting down the turtle after hes done.
		System.out.println("Shutting down - Fuel left: " + miningTurtle.getFuelLevel());
		miningTurtle.shutdown();

	}
}
