package org.hoppy.miningturtle.wrappers;

public abstract class Block {

	protected int x, y, z;
	
	public Block(String name, int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	//Setters
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public void setZ(int z){
		this.z = z;
	}
	
	
	//Getters
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	public int getZ(){
		return z;
	}
	
}
