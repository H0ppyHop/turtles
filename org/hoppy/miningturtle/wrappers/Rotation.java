package org.hoppy.miningturtle.wrappers;

public enum Rotation {
	SOUTH("South" ,0),
	WEST("West", 1),
	NORTH("North", 2),
	EAST("East", 3);
	
	public static final Rotation[] ROTATIONS = Rotation.values();
	
	final String name;
	final int value;
	
	Rotation(String name, int value){
		this.name = name;
		this.value = value;
	}
	
	public String getName(){
		return name;
	}
	
	public int getValue(){
		return value;
	}
	
}
